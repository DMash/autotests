import unittest
import HtmlTestRunner
import os 

# get the directory path to output report file
report_dir = './reports'
# get all tests from test cases folder
tests = unittest.TestLoader().discover('.', pattern = "tc_*.py")
print(tests.countTestCases())
# configure HTMLTestRunner options
runner = HtmlTestRunner.HTMLTestRunner(output=report_dir, combine_reports=True, report_name="yahoo_tests", add_timestamp=True)
# run the suite using HTMLTestRunner
runner.run(tests)
