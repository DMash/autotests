import unittest, xlrd
from ddt import ddt, data, unpack
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

def get_data(file_name):
    # create an empty list to store rows
    rows = []
    # open the specified Excel spreadsheet as workbook
    book = xlrd.open_workbook(file_name)
    # get the first sheet
    sheet = book.sheet_by_index(0)
    # iterate through the sheet and get data from rows in list
    for row_idx in range(1, sheet.nrows):
        rows.append(list(sheet.row_values(row_idx, 0, sheet.ncols)))
    print(rows)
    return rows

@ddt
class SportsLeaderboardTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.maximize_window()
        cls.driver.get("https://yahoo.com/")


    # specify test data using @data decorator
    @data(*get_data("test_data_tc_sports.xlsx"))
    @unpack
    def test_sports_leaderboard(self, keyword):
        sports_link = self.driver.find_element_by_link_text("Sports")
        sports_link.click()
        self.assertTrue(WebDriverWait(self.driver, 10).until(EC.title_contains("Yahoo! Sports")) and \
                   WebDriverWait(self.driver, 10).until(EC.url_contains("sports.")) and \
                   WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT, keyword))))
        
        sport_link = self.driver.find_element_by_link_text(keyword)
        sport_link.click()
        sport_title_text = keyword + " " + "News"
        sport_link_text = "/" + keyword.lower()
        sport_home_link_text = keyword + " " + "Home"
        self.assertTrue(WebDriverWait(self.driver, 10).until(EC.title_contains(sport_title_text)) and \
                   WebDriverWait(self.driver, 10).until(EC.url_contains(sport_link_text)) and \
                   WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT, sport_home_link_text))))
        

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == "__main__":
    unittest.main(verbosity=2)
