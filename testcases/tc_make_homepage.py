import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

class MakeHomepageTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.maximize_window()
        cls.driver.get("https://yahoo.com/")

    def test_homepage_banner_presence(self):
        self.assertTrue(self.is_element_present(By.ID,"mega-banner"))

    def test_set_button_presence(self):
        self.assertTrue(self.is_element_present(By.ID, "mega-banner-add"))

    def test_set_as_homepage(self):
        set_homepage_button = self.driver.find_element_by_id("mega-banner-add")
        set_homepage_button.click()
        WebDriverWait(self.driver, 10).until(lambda s: "#extInstall?partner=oo-hpbanner" in s.current_url)
        self.assertIn("#extInstall?partner=oo-hpbanner", self.driver.current_url)     

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def is_element_present(self, how, what):
        """
        Utility method to check presence of an element on page
        :params how: By locator type
        :params what: locator value
        """
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException:
            return False
        return True

if __name__ == "__main__":
    unittest.main(verbosity=2)
