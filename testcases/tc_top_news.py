import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

class TopNewsTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.maximize_window()
        cls.driver.get("https://yahoo.com/")


    def test_top_news(self):
        news_link = self.driver.find_element_by_link_text("News")
        news_link.click()

        self.assertTrue(WebDriverWait(self.driver, 10).until(EC.title_contains("Latest News & Headlines")) and \
                   WebDriverWait(self.driver, 10).until(EC.url_contains("news.")))

        top_news = self.driver.find_elements_by_xpath("//div[@id='item-0']//descendant::a[not(starts-with(@class, 'comment-btn-link'))]")
        self.assertEqual(6, len(top_news))

        top_link = top_news[0].get_attribute("href")
        self.driver.get(top_link)

        self.assertTrue(WebDriverWait(self.driver, 10).until(EC.title_contains(self.driver.find_element_by_xpath("//header[@id='Lead-1-HeadComponentTitle']/h1").text)) \
                                                   and 'err=404' not in self.driver.current_url)
              

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == "__main__":
    unittest.main(verbosity=2)
