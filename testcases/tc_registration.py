import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from time import gmtime, strftime
from random import randint
from selenium.webdriver.support.ui import Select

class RegistrationTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        cls.driver.maximize_window()
        cls.driver.get("https://yahoo.com/")


    def test_registration(self):
        sign_in_link = self.driver.find_element_by_id("uh-signin")
        sign_in_link.click()

        create_account_link = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID,"createacc")))
        create_account_link.click()

        WebDriverWait(self.driver, 10).until(lambda s: "account/create?" in s.current_url)

        # fields determination
        first_name = self.driver.find_element_by_id("usernamereg-firstName")
        last_name = self.driver.find_element_by_id("usernamereg-lastName")
        email = self.driver.find_element_by_id("usernamereg-yid")
        password = self.driver.find_element_by_id("usernamereg-password")
        mob_code = Select(self.driver.find_element_by_xpath("//select[@name='shortCountryCode']"))
        phone = self.driver.find_element_by_id("usernamereg-phone")
        birth_month = Select(self.driver.find_element_by_id("usernamereg-month"))
        birth_day = self.driver.find_element_by_id("usernamereg-day")
        birth_year = self.driver.find_element_by_id("usernamereg-year")
        gender = self.driver.find_element_by_id("usernamereg-freeformGender")
        continue_button = self.driver.find_element_by_id("reg-submit-button")
        
        # check all fields are enabled
        self.assertTrue(first_name.is_enabled() and last_name.is_enabled()
            and email.is_enabled() and password.is_enabled() and
            phone.is_enabled() and birth_day.is_enabled() and
            birth_year.is_enabled() and gender.is_enabled() and
            continue_button.is_enabled())

        # check fields max length
        self.assertEqual("32", first_name.get_attribute("maxlength"))
        self.assertEqual("32", last_name.get_attribute("maxlength"))
        self.assertEqual("32", email.get_attribute("maxlength"))
        self.assertEqual("128", password.get_attribute("maxlength"))

        # check month dropdown
        self.assertEqual(12+1, len(birth_month.options))

        # check terms links presence
        self.assertTrue(self.driver.find_element_by_link_text("Terms").is_displayed()
                        and self.driver.find_element_by_link_text("Privacy Policy").is_displayed())

        # fill out all the fields
        email_value = "email_" + strftime("%Y%m%d%H%M%S", gmtime())
        first_name.send_keys("TestFN")
        last_name.send_keys(email_value)
        email.send_keys(email_value)
        option = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//ul[@id='desktop-suggestion-list']/li[1]")))
        option.click()
        password.send_keys("Tester!@3")
        mob_code.select_by_value("BY")
        phone.send_keys("291111111")
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located((By.ID, "desktop-suggestions-container")))
        birth_month.select_by_value(str(randint(1,12)))
        birth_day.send_keys(str(randint(1,28)))
        birth_year.send_keys(str(randint(1981,2000)))
        gender.send_keys("correct")

        #click Continue button
        continue_button.click()

        captcha_frame = self.driver.find_element_by_tag_name("iframe")
        self.driver.switch_to.frame(captcha_frame)
        title_captcha = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//div[@id='recaptcha-script']/h1")))
        self.assertEqual("Prove you're not a robot", title_captcha.text)

            
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == "__main__":
    unittest.main(verbosity=2)
