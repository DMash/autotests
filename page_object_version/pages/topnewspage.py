from pages.base import BasePage
from pages.base import InvalidPageException


class TopNewsPage(BasePage):
    _news_caption_locator = "//header[@id='Lead-1-HeadComponentTitle']/h1"

    def __init__(self, driver):
        super(TopNewsPage, self).__init__(driver)

    def _validate_page(self, driver):
        if driver.find_element_by_xpath(self._news_caption_locator).text not in driver.title:
            raise InvalidPageException("Top News Page is not loaded")
