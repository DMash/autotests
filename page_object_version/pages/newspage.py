from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from pages.base import BasePage
from pages.base import InvalidPageException
from random import randrange
from pages.topnewspage import TopNewsPage


class NewsPage(BasePage):
    _page_title = 'Latest News & Headlines'
    _url_part = 'news.'

    _top_news_locator = "//div[@id='item-0'] //descendant::a[not(starts-with(@class, 'comment-btn-link')) " \
                        "and not(starts-with(@class, 'yvp')) and not(contains(@href, 'info.yahoo.com'))]"

    _news = {}

    def __init__(self, driver):
        super(NewsPage, self).__init__(driver)
        self._news = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located(
                                                                (By.XPATH, self._top_news_locator)))

    def _validate_page(self, driver):
        if self._page_title not in driver.title or self._url_part not in driver.current_url:
            raise InvalidPageException("News Page is not loaded")

    @property
    def top_news_count(self):
        return len(self._news)

    def navigate_to_top_news(self):
        top_link = self._news[randrange(self.top_news_count)].get_attribute("href")
        self.driver.get(top_link)
        return TopNewsPage(self.driver)
