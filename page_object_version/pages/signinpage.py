from pages.base import BasePage
from pages.base import InvalidPageException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.registrationpage import RegistrationPage


class SignIn(BasePage):

    _registration_link_locator = 'createacc'
    _login_page_url = 'login.'
    _login_page_title = 'login'

    def __init__(self, driver):
        super(SignIn, self).__init__(driver)
        self.registration_link = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
            (By.ID, self._registration_link_locator)))

    def _validate_page(self, driver):
        if self._login_page_url not in driver.current_url or self._login_page_title not in driver.title:
            raise InvalidPageException("Sign In Page is not loaded")

    def navigate_to_registration(self):
        self.registration_link.click()
        return RegistrationPage(self.driver)
