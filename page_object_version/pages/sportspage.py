from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from pages.base import BasePage
from pages.base import InvalidPageException


class SportsPage(BasePage):
    _page_title = 'Yahoo! Sports'
    _url_part = 'sports.'

    def __init__(self, driver, key_sport):
        super(SportsPage, self).__init__(driver)
        self.key_sport = key_sport

    def _validate_page(self, driver):
        if self._page_title not in driver.title or self._url_part not in driver.current_url:
            raise InvalidPageException("Sports Page is not loaded")

    def navigate_to_sport(self):
        sport_link = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT, self.key_sport)))
        sport_link.click()
        WebDriverWait(self.driver, 20).until(EC.visibility_of(self.sport_home_link_text))
        return

    @property
    def sport_title_text(self):
        return self.key_sport + " " + "News"

    @property
    def sport_link_text(self):
        return "/" + self.key_sport.lower()

    @property
    def sport_home_link_text(self):
        return WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                        (By.LINK_TEXT, self.key_sport + " " + "Home")))
