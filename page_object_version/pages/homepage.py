from pages.homepagegroup import HomePageGroup
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.newspage import NewsPage
from pages.signinpage import SignIn
from pages.sportspage import SportsPage


class MakeHomeRegion(HomePageGroup):

    _make_homepage_banner_locator = 'amplify-hpset'

    def __init__(self, driver):
        super(MakeHomeRegion, self).__init__(driver)

    @property
    def home_page_banner(self):
        return self.driver.find_element(By.ID, self._make_homepage_banner_locator)

    def make_browser_home_page(self):
        WebDriverWait(self.driver, 10).until(lambda s: "#extInstall?partner=oo-hpbanner" in s.current_url)
        return self.driver.current_url


class SignInRegion(HomePageGroup):

    _sign_in_link_locator = 'Sign in'

    def __init__(self, driver):
        super(SignInRegion, self).__init__(driver)
        self.sign_in_link = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                                                        (By.LINK_TEXT, self._sign_in_link_locator)))

    def navigate(self):
        self.sign_in_link.click()
        return SignIn(self.driver)


class HomePage(HomePageGroup):

    _news_link_locator = 'News'
    _sports_link_locator = 'Sports'

    def __init__(self, driver):
        super(HomePage, self).__init__(driver)
        self.news_link = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
            (By.LINK_TEXT, self._news_link_locator)))
        self.sports_link = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
            (By.LINK_TEXT, self._sports_link_locator)))

    def navigate_to_news(self):
        self.news_link.click()
        return NewsPage(self.driver)

    def navigate_to_sports(self, key_sport):
        self.sports_link.click()
        return SportsPage(self.driver, key_sport)
