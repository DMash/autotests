from pages.base import BasePage
from pages.base import InvalidPageException


class HomePageGroup(BasePage):
    _logo_locator = '//header/div[2]/div/div/div[1]/div[1]/h1/a'
    _homepage_caption = 'Yahoo'

    def __init__(self, driver):
        super(HomePageGroup, self).__init__(driver)

    def _validate_page(self, driver):
        if not driver.title == self._homepage_caption:
            raise InvalidPageException("Home Page is not loaded")

    """ Regions define functionality available through all home
    page group objects """
    @property
    def sign_in(self):
        from pages.homepage import SignInRegion
        return SignInRegion(self.driver)

    @property
    def make_home(self):
        from pages.homepage import MakeHomeRegion
        return MakeHomeRegion(self.driver)
