from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.base import BasePage
from pages.base import InvalidPageException
from time import gmtime, strftime
from random import randint
from selenium.webdriver.support.ui import Select


class RegistrationPage(BasePage):
    _partial_create_link = 'account/create?'

    _first_name_locator = 'usernamereg-firstName'
    _last_name_locator = 'usernamereg-lastName'
    _email_locator = 'usernamereg-yid'
    _password_locator = 'usernamereg-password'
    _mob_code_locator = "//select[@name='shortCountryCode']"
    _phone_locator = "usernamereg-phone"
    _birth_month_locator = "usernamereg-month"
    _birth_day_locator = "usernamereg-day"
    _birth_year_locator = "usernamereg-year"
    _gender_locator = "usernamereg-freeformGender"
    _continue_button_locator = "reg-submit-button"
    _terms_locator = "Terms"
    _privacy_link_locator = "Privacy Policy"
    _option_value_locator = "//ul[@id='desktop-suggestion-list']/li[1]"
    _suggestions_locator = "desktop-suggestions-container"
    _captcha_title_locator = "//div[@id='recaptcha-script']/h1"

    def __init__(self, driver):
        super(RegistrationPage, self).__init__(driver)
        self.first_name = driver.find_element_by_id(self._first_name_locator)
        self.last_name = driver.find_element_by_id(self._last_name_locator)
        self.email = driver.find_element_by_id(self._email_locator)
        self.password = driver.find_element_by_id(self._password_locator)
        self.mob_code = Select(driver.find_element_by_xpath(self._mob_code_locator))
        self.phone = driver.find_element_by_id(self._phone_locator)
        self.birth_month = Select(driver.find_element_by_id(self._birth_month_locator))
        self.birth_day = driver.find_element_by_id(self._birth_day_locator)
        self.birth_year = driver.find_element_by_id(self._birth_year_locator)
        self.gender = driver.find_element_by_id(self._gender_locator)
        self.continue_button = driver.find_element_by_id(self._continue_button_locator)
        self.terms_link = driver.find_element_by_link_text(self._terms_locator)
        self.privacy_link = driver.find_element_by_link_text(self._privacy_link_locator)

    def _validate_page(self, driver):
        if self._partial_create_link not in driver.current_url:
            raise InvalidPageException("Registration Page is not loaded")

    @property
    def fields_interaction_possibility(self):
        return self.first_name.is_enabled() and self.last_name.is_enabled() and self.email.is_enabled() and \
            self.password.is_enabled() and self.phone.is_enabled() and self.birth_day.is_enabled() and \
            self.birth_year.is_enabled() and self.gender.is_enabled() and self.continue_button.is_enabled() and \
            self.terms_link.is_displayed() and self.privacy_link.is_displayed()

    @property
    def first_name_length(self):
        return self.first_name.get_attribute("maxlength")

    @property
    def last_name_length(self):
        return self.last_name.get_attribute("maxlength")

    @property
    def email_length(self):
        return self.email.get_attribute("maxlength")

    @property
    def password_length(self):
        return self.password.get_attribute("maxlength")

    @property
    def month_count(self):
        return len(self.birth_month.options)

    def fill_form(self):
        email_value = "email_" + strftime("%Y%m%d%H%M%S", gmtime())
        self.first_name.send_keys("TestFN")
        self.last_name.send_keys(email_value)
        self.email.send_keys(email_value)
        option = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                                                    (By.XPATH, self._option_value_locator)))
        option.click()
        self.password.send_keys("Tester!@3")
        self.mob_code.select_by_value("BY")
        self.phone.send_keys("291111111")
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located((By.ID, self._suggestions_locator)))
        self.birth_month.select_by_value(str(randint(1, 12)))
        self.birth_day.send_keys(str(randint(1, 28)))
        self.birth_year.send_keys(str(randint(1981, 2000)))
        self.gender.send_keys("correct")
        self.continue_button.click()
        captcha_frame = self.driver.find_element_by_tag_name("iframe")
        self.driver.switch_to.frame(captcha_frame)
        return WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                                (By.XPATH, self._captcha_title_locator)))
