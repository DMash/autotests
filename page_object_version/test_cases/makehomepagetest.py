import unittest
from test_cases.basetestcase import BaseTestCase
from pages.homepage import HomePage


class MakeHomePageTest(BaseTestCase):
    def test_homepage_banner_presence(self):
        homepage = HomePage(self.driver)
        self.assertTrue(homepage.make_home.home_page_banner.is_displayed())

    def test_set_as_homepage(self):
        homepage = HomePage(self.driver)
        #self.assertIn("#extInstall?partner=oo-hpbanner", homepage.make_home.make_browser_home_page())
        pass


if __name__ == '__main__':
    unittest.main(verbosity=2)
