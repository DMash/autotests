from test_cases.basetestcase import BaseTestCase
import unittest
from pages.homepage import HomePage


class RegistrationTest(BaseTestCase):
    def test_registration(self):
        homepage = HomePage(self.driver)
        registration_form = homepage.sign_in.navigate().navigate_to_registration()
        self.assertTrue(registration_form.fields_interaction_possibility)
        self.assertEqual("32", registration_form.first_name_length)
        self.assertEqual("32", registration_form.last_name_length)
        self.assertEqual("32", registration_form.email_length)
        self.assertEqual("128", registration_form.password_length)
        self.assertEqual(12 + 1, registration_form.month_count)
        captcha_title = registration_form.fill_form()
        self.assertEqual("Prove you're not a robot", captcha_title.text)


if __name__ == '__main__':
    unittest.main(verbosity=2)