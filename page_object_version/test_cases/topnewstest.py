import unittest
from test_cases.basetestcase import BaseTestCase
from pages.homepage import HomePage


class TopNewsTest(BaseTestCase):

    def test_top_news(self):
        homepage = HomePage(self.driver)
        news_page = homepage.navigate_to_news()
        self.assertEqual(6, news_page.top_news_count)
        news_page.navigate_to_top_news()
        self.assertTrue('err=404' not in self.driver.current_url)


if __name__ == '__main__':
    unittest.main(verbosity=2)
