from test_cases.basetestcase import BaseTestCase
import unittest
from pages.homepage import HomePage
import xlrd
from ddt import ddt, data, unpack


def get_data(file_name):
    # create an empty list to store rows
    rows = []
    # open the specified Excel spreadsheet as workbook
    book = xlrd.open_workbook(file_name)
    # get the first sheet
    sheet = book.sheet_by_index(0)
    # iterate through the sheet and get data from rows in list
    for row_idx in range(1, sheet.nrows):
        rows.append(list(sheet.row_values(row_idx, 0, sheet.ncols)))
    print(rows)
    return rows


@ddt
class SportsLeaderboardTest(BaseTestCase):

    @data(*get_data("test_data/test_data_tc_sports.xlsx"))
    @unpack
    def test_sports(self, keyword):
        homepage = HomePage(self.driver)
        sports_page = homepage.navigate_to_sports(keyword)
        sports_page.navigate_to_sport()
        self.assertTrue(sports_page.sport_title_text in self.driver.title)
        self.assertTrue(sports_page.sport_link_text in self.driver.current_url)
        self.assertTrue(sports_page.sport_home_link_text.is_displayed())


if __name__ == '__main__':
    unittest.main(verbosity=2)